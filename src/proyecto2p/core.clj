(ns proyecto2p.core
  (:gen-class)
  (:import [libsvm svm_node svm_model svm_parameter svm_problem])
  (:use   [incanter core stats datasets charts io])
  (:require [svm.core :as svm])
)

(import '(javax.swing JFrame JLabel JTextField JButton JOptionPane)
        '(java.awt.event ActionListener)
        '(java.awt GridLayout)
)

(defn -main [& args]
  (def dat-file (clojure.java.io/resource "data.dat" ))
  (def csv-file (clojure.java.io/resource "data.csv" ))

  (def tipsTh (list "Dentro de 5 minutos se lo invita a refrescarse en el auditorio"
                    "En breve se los invitara al bar a tomarse unos refrescos bien frios"
                    "Debido al intenso calor se activara el aire acondicionado"))

  (def tipsRuido (list "Debido al ruido hoy saldremos mas temprano de lo previsto"
                        "En breve nos haremos cargo del ruido"
                        "Por favor mantenga la calma, ya se tomaron medidas para quitar el ruido"))

  (def ds1 (svm/read-dataset dat-file))
  (def ds2 (to-matrix (read-dataset csv-file :header true)))
  
  (def model (svm/train-model ds1))

  (let [  frame (new JFrame "Application")
          lblTemperatura (new JLabel "Temperatura:")
          txtTemperatura (new JTextField 10)
          lblHumedad (new JLabel "Humedad:")
          txtHumedad (new JTextField 10)
          lblRuido (new JLabel "Ruido")
          txtRuido (new JTextField 10)
          btnCalcular (new JButton "Calcular")
          btnBoxplot (new JButton "Mostrar grafico")]

      (. btnCalcular (addActionListener
          (proxy [ActionListener] []
              (actionPerformed [evt]
                  (def label "Condicion Ambiental: ")
                  (def temperatura (Double/parseDouble (. txtTemperatura (getText))))
                  (def humedad (Double/parseDouble (. txtHumedad (getText))))
                  (def ruido (Double/parseDouble (. txtRuido (getText))))
                  (def test (sorted-map 1 temperatura 2 humedad 3 ruido))
                  (def categoria (svm/predict model test))
                  (cond
                    (= categoria 0.0) (def msg (str label "Adecuada"))
                    (= categoria 1.0) (def msg (str label (str "No adecuada (Th)\n" (nth tipsTh (rand-int 3)) )))
                    (= categoria 2.0) (def msg (str label (str "No adecuada (Ruido)\n" (nth tipsRuido (rand-int 3)) )))
                    :else (def msg (str label "Desconocido")))
                  (JOptionPane/showMessageDialog nil msg)
              )
          )    
      ))

      (. btnBoxplot (addActionListener
        (proxy [ActionListener] []
            (actionPerformed [evt]
                ; BOXPLOT PARA CADA CATEGORIA DE TEMPERATURAS
                (def temperaturas (group-on ds2 0 :cols 1))
                (doto (box-plot (first temperaturas) 
                  :title "Temperaturas (°C)"
                  :legend true)
                    (add-box-plot (second temperaturas))
                    (add-box-plot (last temperaturas))
                    view)

                ; BOXPLOT PARA EL RUIDO POR CADA CATEGORIA
                (def ruidos (group-on ds2 0 :cols 3))
                (doto (box-plot (first ruidos) 
                  :title "Ruidos (dB)"
                  :legend true)
                    (add-box-plot (second ruidos))
                    (add-box-plot (last ruidos))
                    view)
            )
        )    
      ))
      
      (doto frame
          (.add lblTemperatura)
          (.add txtTemperatura)
          (.add lblHumedad)
          (.add txtHumedad)
          (.add lblRuido)
          (.add txtRuido)
          (.add btnCalcular)
          (.add btnBoxplot)

          (.setLayout (new GridLayout 4 2 4 4))
          (.setDefaultCloseOperation (JFrame/EXIT_ON_CLOSE)) 
          (.setSize 300 150)
          (.setResizable false)
          (.setLocationRelativeTo nil)
          (.setVisible true)
      )
  )
)
